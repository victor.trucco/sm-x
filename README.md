# This port of One chip MSX (OCM) for SM-X

The One chip MSX, or 1chipMSX as the D4 Enterprise distributional name for the ESE MSX System 3,
is a re-implementation of an MSX home computer that uses a single FPGA to implement all the electronics of an MSX and some extra interfaces.

Based on lastest version of Luca Chiodi (KdL) port
https://gnogni.altervista.org/

## Front Buttons

Button 1 (left button): Reset

Button 2 (right button): Scanlines 25% -> 50% -> 75% -> Off


## Dip Switchs

![Dip Switchs](dipswitch8.jpg)

![Switch table](switch-table.png)

# WiFi

## Creating a SD card with internet access

Oduvaldo Pavan Junior video tutorial, portuguese audio with english subtitles.

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=h2RQ-0zdfQ8&feature=youtu.be)

Links mentioned on the video:

<b>OCM-SDBIOS Pack:</b>

Go to the https://gnogni.altervista.org/
then click the "OCM-SDBIOS Pack", send the e-mail and wait for the KdL contact you with the link to the file.

<b>MSX-SM UNAPI PACK:</b>

Lastest Oduvaldo's UNAPI pack

https://drive.google.com/file/d/1ZiuohZ11IX0OoMj3D_qFwgpdUDMUr2Rb/view

## Setting or changing your WiFi password

Oduvaldo Pavan Junior video tutorial, portuguese audio with english subtitles.

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=F0Wecd8xxQ8&feature=youtu.be)
