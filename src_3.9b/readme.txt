OCM-PLD Pack v3.9
=================

Author:  KdL
Update:  2021.08.23

This package includes a set of custom firmware to update the following machines.

1st Gen  =>  1chipMSX, Zemmix Neo (KR), Zemmix Neo BR, SX-1 and SX-1 Mini/Mini+.
2nd Gen  =>  SM-X , SM-X Mini and SX-2.

All firmware are unofficial, as they are not offered by their respective manufacturers.
Any content of this package is NOT intended for a commercial purpose and is for personal use only.
However, manufacturers are allowed to use these firmware as long as no surcharge is applied.
The author disclaims any liability for the misuse of the contents of this package.

The common basis firmware is from the first generation OCM-PLD firmware, born with 1chipMSX.
The second generation machines have additional features since they are more capacious.
For more details, refer to the 'history.txt' and the other texts included in the subfolders.


______
Enjoy!
