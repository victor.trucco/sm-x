# This port of One chip MSX (OCM) for SM-X / SM-X Mini / SM-X HB

The One chip MSX, or 1chipMSX as the D4 Enterprise distributional name for the ESE MSX System 3,
is a re-implementation of an MSX home computer that uses a single FPGA to implement all the electronics of an MSX and some extra interfaces.

Based on version 3.9 of Luca Chiodi (KdL) port
https://gnogni.altervista.org/

This 3.9 port has additional features over KdL 3.9 and was made by Ducasp:
https://github.com/ducasp/

With help from Victor Trucco to adapt KdL 3.9 implementation for SM-X HB and SM-X Mini, including SDRAM controller improvements found on 3.7.1 build by Victor Trucco.

## Front Buttons (SM-X HB use Hotbit Reset button and Scanlines button is internal)

Button 1 (left button): Reset

Button 2 (right button): Scanlines 25% -> 50% -> 75% -> Off

SM-X HB easy switch of scanlines: use SMARTCMD -8x where x goes from 0 to 3. 0 turns off scanlines, 1 uses 25%, 2 uses 50% and 3 uses 75% scanlines effect.

## Dip Switchs

![Dip Switchs](dipswitch8.jpg)

![Switch table](switch-table.png)

Note: SM-X HB has 4 extra positions in its DIP-Switch, only position 9 is being currently used.

# WiFi

## Creating a SD card with internet access

Oduvaldo Pavan Junior video tutorial, portuguese audio with english subtitles.

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=h2RQ-0zdfQ8&feature=youtu.be)

Links mentioned on the video:

<b>OCM-SDBIOS Pack:</b>

Go to the https://gnogni.altervista.org/
then click the "OCM-SDBIOS Pack", send the e-mail and wait for the KdL contact you with the link to the file.

<b>MSX-SM UNAPI PACK:</b>

Lastest Oduvaldo's UNAPI pack

https://drive.google.com/file/d/1ZiuohZ11IX0OoMj3D_qFwgpdUDMUr2Rb/view

## Setting or changing your WiFi password

Oduvaldo Pavan Junior video tutorial, portuguese audio with english subtitles.

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=F0Wecd8xxQ8&feature=youtu.be)

## Extras over KdL OCM 3.9

- All devices and builds
    - Improved SDRAM controller by Victor Trucco
- All devices and builds EXCEPT SM-X HB (all types) builds
    - OPL3 fixes that allows VGMPLAY 1.3 to work properly when OPL3 is used
        - Fixed not having interrupts from OPL3 sent to the MSX BUS
        - Fixed OPL3 timers timing being off
    - OPL3 improvement in quality
        - Due to FPGA space constraints, we can render only one output channel (left is being used), as sequencing for each channel uses quite some resources and doesn't fit when both are sequenced
        - Original solution was to connect only Left Output to speakers. Issue is that channels directed to play only on Right channel won't be heard
        - I've added a "mono" pin to the virtual OPL3, and that will cause any channels direct to play only on one channel to be played on both channels, so when mono pin is active, all sounds play on Left Channel
        - Try Doom VGM sound track 3 as an example where you would have large moments of silence before the fix and missing sounds/effects
- All devices and builds EXCEPT SM-X HB Frankysnd
    - VAUS (Arkanoid) and Standard MSX (Galaga Paddle Patch/Basic/HIDTest/etc) emulation over PS/2 mouse
        - Off by Default. Once turned on, will persist until it is turned off or you power down and then power up the SM-X
        - VAUS Mode: activate it using SETSMART -8E. Joystick port 1 external connection and MSX Mouse emulation will be turned off.
            - Mouse movements left and right: decrease/increase paddle position
            - Mouse buttons 1 and 2: VAUS Paddle button (it has only one button)
            - Mouse button 3: Change sensitivity to mouse movement, 4 levels available
        - MSX Mode: activate it using SETSMART -8F. Joystick port 1 external connection and MSX Mouse emulation will be turned off.
            - Mouse movements left and right: decrease/increase paddle position
            - Mouse buttons 1 and 2: MSX Paddle buttons 1 and 2
            - Mouse button 3: Change sensitivity to mouse movement, 4 levels available
            - *IMPORTANT*: MSX Standard Paddle works through tight timing, and most software using it read it through BIOS functions. Those functions are hardcoded for a ~3.58MHz z80 CPU, so it will work best with Turbo disabled. With turbo enabled it will be much more sensitive and also will have jerky movements.
- SM-X HB Only
    - Select Key fixed, now it works
    - Shortcuts emulate Page Up / Page Down and F9 to F12 with Hotbit Keyboard, allowing most OCM shortcuts for settings to be used by the keyboard
        - Page Up: Select and +
        - Page Down: Select and -
        - F9 to F12: Select and F1 to F4
    - Joystick port debounce can be disable, try that if a device connected on the joystick port is not working properly
        - SETSMART -89 will disable debounce
        - SETSMART -8A will re-enable it (default after powering on as well)
    - DIP Switch position 9 to select proper Keyboard Map when using external PS/2 Keyboard
        - By default, leave it on OFF. It will work properly for the Hotbit Keyboard
        - If you make the changes to connect an external keyboard, leave it on ON. It will work properly for PS/2 Keyboard
        - *IMPORTANT*: leaving it ON when using Hotbit keyboard will cause Keyboard to not work properly, make sure to have it OFF if using Hotbit keyboard
- Frankysnd builds for all devices:
    - I've added partial support to a built-in Franky. It works with SG1000, COL4MMM (using COM\Franky versions) VGMPLAY and Sofarun (remember to set it to use MSX VDP and Franky's PSG). As Franky sound uses I/O ports 0x48 and 0x49, and those ports are part of the switched I/O, it is usually disabled, as OCM IPL loader will leave switched I/O selected after booting. There are different ways to enable Franky sound:
        - VGMPLAY will automatically disable switched I/O, so you can play a VGM song that uses SN76489 and after exiting VGMPLAY you can use other software.
        - De-select the internal switched I/O by sending the basic command OUT &H40,0
        - Use SETSMART -8C to reserve I/O ports 0x48 and 0x49 for Franky no matter what, so any program relying on reading OCM information on those ports won't get it.
    - I've added s second PSG on ports 0x10 to 0x13.
        - No extra configuration needed, always enabled
        - Try Maze of Galious patched to work on MSX2 (and remember to enable SCC on SLOT 1 or on SLOT 2). It has a really amazing sound track using two PSG's and SCC :)

## How to build it? (not necessary, only for those that want to build it themselves or modify it)

I recommend using at least Intel Quartus Prime 17.1, latest version of Quartus Prime works fine as well, it fixes quite a lot of issues that Altera Quartus 13 had.

- Test builds
    - go to OCM_SM folder
    - run !!-cleanup.cmd
    - run !!-init-???.cmd (where ??? is the name of the device being built and the build type, if any)
    - run 1_sm_compile.cmd, it will open Quartus Prime and you can build it there
    - once build is finished, you can use the files in OUTPUT_FILES folder to send through USB Blaster and test without flashing
    - if you want to create a PLD and JIC file to send to someone to test directly
        - In Quartus select File, and then Convert Programming Files...
        - Click Open Conversion Setup Data... button and select one of the .cof files, I usually use ocm_sm_512k_dual_epbios_backslash.cof
        - Click Generate button, it will tell you the JIC file has been created
        - Now, execute 2_sm_finalize.cmd and then 3_sm_collect.cmd, JIC and PLD files will be in the FW folder

- Once all is good and you want to batch build all variants:
    - go to OCM_SM folder
    - run !!-cleanup.cmd
    - run !!-init-???.cmd (where ??? is the name of the device being built and the build type, if any)
    - run 1_sm_mega-build_and_collect.cmd, it will build it automatically for ALL combinations scenarios
    - once build is finished, you can collect all files properly zipped and named in PACKED_FILES folder
    - rinse and repeat for other device/buid type combination you want to do (but remember to copy files from PACKED_FILES folder, as it will hold the files for the new build, you have a buffer of one build, if you forget, in OLD_PACKED_FILES folder)