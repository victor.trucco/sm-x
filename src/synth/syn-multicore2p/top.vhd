

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
    port 
    (
   -- Clocks
        clock_50_i         : in    std_logic;

        -- Buttons
        btn_n_i            : in    std_logic_vector(4 downto 1);

        -- SRAM
        sram_addr_o        : out   std_logic_vector(20 downto 0)   := (others => '0');
        sram_data_io       : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram_we_n_o        : out   std_logic                               := '1';
        sram_oe_n_o        : out   std_logic                               := '1';

        -- SDRAM
        SDRAM_A            : out std_logic_vector(12 downto 0);
        SDRAM_DQ           : inout std_logic_vector(15 downto 0);

        SDRAM_BA           : out std_logic_vector(1 downto 0);
        SDRAM_DQMH         : out std_logic;
        SDRAM_DQML         : out std_logic;    

        SDRAM_nRAS         : out std_logic;
        SDRAM_nCAS         : out std_logic;
        SDRAM_CKE          : out std_logic;
        SDRAM_CLK          : out std_logic;
        SDRAM_nCS          : out std_logic;
        SDRAM_nWE          : out std_logic;
    
        -- PS2
        ps2_clk_io         : inout std_logic                        := 'Z';
        ps2_data_io        : inout std_logic                        := 'Z';
        ps2_mouse_clk_io   : inout std_logic                        := 'Z';
        ps2_mouse_data_io  : inout std_logic                        := 'Z';

        -- SD Card
        sd_cs_n_o          : inout  std_logic                        := 'Z';
        sd_sclk_o          : out    std_logic                        := 'Z';
        sd_mosi_o          : out    std_logic                        := 'Z';
        sd_miso_i          : inout  std_logic;

        -- Joysticks
        joy_clock_o        : out   std_logic;
        joy_load_o         : out   std_logic;
        joy_data_i         : in    std_logic;
        joy_p7_o           : out   std_logic                        := '1';

        -- Audio
        AUDIO_L             : out   std_logic                       := '0';
        AUDIO_R             : out   std_logic                       := '0';
        ear_i               : in    std_logic;
        mic_o               : out   std_logic                       := '0';

        -- VGA
        VGA_R               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_G               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_B               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_HS              : out   std_logic                       := '1';
        VGA_VS              : out   std_logic                       := '1';

        LED                 : out   std_logic                       := '1';-- 0 is led on

        --STM32
        stm_rx_o            : out std_logic     := 'Z'; -- stm RX pin, so, is OUT on the slave
        stm_tx_i            : in  std_logic     := 'Z'; -- stm TX pin, so, is IN on the slave
        stm_rst_o           : out std_logic     := 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
        
        SPI_SCK             : in  std_logic;
        SPI_DO              : out std_logic   := 'Z';
        SPI_DI              : in  std_logic;
        SPI_SS2             : in  std_logic;
        SPI_nWAIT           : out std_logic   := '1';

        GPIO                : inout std_logic_vector(31 downto 0)   := (others => 'Z')
    );
end entity;

architecture Behavior of top is




    type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);
    signal config_buffer_s : config_array;

    function to_slv(s: string) return std_logic_vector is
        constant ss: string(1 to s'length) := s;
        variable rval: std_logic_vector(1 to 8 * s'length);
        variable p: integer;
        variable c: integer;

        begin  
        for i in ss'range loop
        p := 8 * i;
        c := character'pos(ss(i));
        rval(p - 7 to p) := std_logic_vector(to_unsigned(c,8));
        end loop;
        return rval;

    end function;

    component joystick_serial is
    port
    (
        clk_i           : in  std_logic;
        joy_data_i      : in  std_logic;
        joy_clk_o       : out  std_logic;
        joy_load_o      : out  std_logic;

        joy1_up_o       : out std_logic;
        joy1_down_o     : out std_logic;
        joy1_left_o     : out std_logic;
        joy1_right_o    : out std_logic;
        joy1_fire1_o    : out std_logic;
        joy1_fire2_o    : out std_logic;
        joy2_up_o       : out std_logic;
        joy2_down_o     : out std_logic;
        joy2_left_o     : out std_logic;
        joy2_right_o    : out std_logic;
        joy2_fire1_o    : out std_logic;
        joy2_fire2_o    : out std_logic
    );
    end component;

    component data_io
    generic
    (
       STRLEN : integer :=   0
    );
    port
    (
        clk_sys                   : in std_logic;
        SPI_SCK, SPI_SS2, SPI_DI :in std_logic;
        SPI_DO : out std_logic;

        data_in       : in std_logic_vector(7 downto 0);
        conf_str      : in std_logic_vector((8*STRLEN)-1 downto 0);
        status        : out std_logic_vector(31 downto 0);

        config_buffer_o : out config_array;



      --  clkref_n          : in  std_logic := '0';
        ioctl_download    : out std_logic;
        ioctl_index       : out std_logic_vector(7 downto 0);
        ioctl_wr          : out std_logic;
        ioctl_addr        : out std_logic_vector(24 downto 0);
        ioctl_dout        : out std_logic_vector(7 downto 0)
    );
    end component;

    component mist_video
    generic (
        OSD_COLOR    : std_logic_vector(2 downto 0) := "110";
        OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
        OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
        SD_HCNT_WIDTH: integer := 9;
        COLOR_DEPTH  : integer := 6;
        OSD_AUTO_CE  : boolean := true;
        USE_FRAMEBUFFER :integer := 0
    );
    port (
        clk_sys     : in std_logic;

        SPI_SCK     : in std_logic;
        SPI_SS3     : in std_logic;
        SPI_DI      : in std_logic;

        scanlines   : in std_logic_vector(1 downto 0);
        ce_divider  : in std_logic := '0';
        scandoubler_disable : in std_logic;

        rotate      : in std_logic_vector(1 downto 0);
        blend       : in std_logic := '0';

        HSync       : in std_logic;
        VSync       : in std_logic;
        R           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
        G           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
        B           : in std_logic_vector(COLOR_DEPTH-1 downto 0);

        VGA_HS      : out std_logic;
        VGA_VS      : out std_logic;
        VGA_R       : out std_logic_vector(4 downto 0);
        VGA_G       : out std_logic_vector(4 downto 0);
        VGA_B       : out std_logic_vector(4 downto 0);
        osd_enable      : out std_logic
    );
    end component mist_video;

    component osd     
    port(
        clk_sys: in std_logic;
        ce: in std_logic;
        
        SPI_SCK: in std_logic;
        SPI_SS3: in std_logic;
        SPI_DI: in std_logic;

        rotate : in std_logic_vector(1 downto 0);

        R_in : in std_logic_vector(5 downto 0);
        G_in : in std_logic_vector(5 downto 0);
        B_in : in std_logic_vector(5 downto 0);
        HSync: in std_logic;
        VSync: in std_logic;

        R_out : out std_logic_vector(5 downto 0);
        G_out : out std_logic_vector(5 downto 0);
        B_out : out std_logic_vector(5 downto 0);
        osd_enable: out std_logic
    );
    end component;

    component PumpSignal
    port(
        clk_i       : in  std_logic;
        reset_i     : in  std_logic;
        download_i  : in  std_logic;   
        pump_o      : out std_logic_vector( 7 downto 0)
    );
    end component;

    -- clocks   
    signal clk21m           : std_logic;
    signal clk_hdmi         : std_logic;        
    signal clk_sdram        : std_logic;        
    

    -- Reset signal
    signal reset_s          : std_logic;        -- Reset geral
    signal power_on_s       : std_logic_vector(26 downto 0) := (others => '1');
    signal power_on_reset   : std_logic := '1';
    
    -- DIPS: similar to Zemmix
    -- bit 7 - 0 = SD enable, 1 = disable
    -- bit 6 - 0 = 4096kb mapper, 1 = 2048kb mapper 
    -- bit 5-4 - config slot 2 "00" - Cart in Slot 2; "10" -SCC2; "01" -ASC8K; "11" -ASC16K
    -- bit 3 - config slot 1 = "0" = cart in slot 1, '1' = megaSCC+ 1024kb
    -- bit 2-1 - video 
    -- bit 0 - cpu speed = 1 = normal - 0 = turbo

    
    -- VGA
    signal vga_r_s              : std_logic_vector(5 downto 0)  := (others => '0');
    signal vga_g_s              : std_logic_vector(5 downto 0)  := (others => '0');
    signal vga_b_s              : std_logic_vector(5 downto 0)  := (others => '0');
    signal vga_hsync_n_s        : std_logic                             := '1';
    signal vga_vsync_n_s        : std_logic                             := '1';
    signal blank_sig            : std_logic;
    
    --audio
    signal SL_s                 : std_logic_vector(5 downto 0)  := (others => '0');
    signal SR_s                 : std_logic_vector(5 downto 0)  := (others => '0');

    -- slot
    signal cpu_ioreq_s      : std_logic;
    signal cpu_mreq_s           : std_logic;
    signal cpu_rd_s         : std_logic;
    signal slot_SLOT1_s     : std_logic;
    signal slot_SLOT2_s     : std_logic;
    signal BusDir_s         : std_logic;
    
    -- HDMI
    signal blank_s              : std_logic;
    signal PCM_s                : std_logic_vector(15 downto 0) := (others => '0'); 

    signal joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i : std_logic;
    signal joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i : std_logic;

    signal blink_s              : std_logic;
    signal pll_locked           : std_logic;
    signal osd_s                : std_logic_vector(7 downto 0);
    signal pump_s               : std_logic_vector(7 downto 0);
    signal status               : std_logic_vector(31 downto 0);

        constant CONF_STR : string :=
 --   "P,OCM_BIOS.DAT;"&
    "D,disable STM SD;"&
    "OAB,Scanlines,Off,25%,50%,75%;"&
    "OC,Blend,Off,On;"&
    "O2,Scandoubler,On,Off;"&
    "O8,VGA Output,CRT,LCD;"&
    "O1,CPU Clock,Normal,Turbo;"&
    "O3,Slot1,Empty,MegaSCC+ 1MB;"&
    "O45,Slot2,Empty,MegaSCC+ 2MB,MegaRAM 1MB,MegaRAM 2MB;"&    
    "O6,RAM,2048kB,4096kB;"&
    "O9,Tape sound,OFF,ON;"&
    "T0,Reset";


        
    --signal dip_s                : std_logic_vector(7 downto 0) := "00100001";       -- Attention! Inverted bits (0 = enabled)

    -- External Slots
    signal slot_A_s : std_logic_vector(15 downto 0);

    alias slot_A0     is sram_addr_o(4);
    alias slot_A1     is sram_addr_o(5);
    alias slot_A2     is sram_addr_o(2);
    alias slot_A3     is sram_addr_o(3);
    alias slot_A4     is sram_addr_o(0);
    alias slot_A5     is sram_addr_o(1);
    alias slot_A6     is sram_addr_o(10);
    alias slot_A7     is sram_addr_o(11);
    alias slot_A8     is sram_addr_o(8);
    alias slot_A9     is sram_addr_o(15);
    alias slot_A10    is sram_addr_o(12);
    alias slot_A11    is sram_addr_o(13);
    alias slot_A12    is sram_addr_o(9);
    alias slot_A13    is sram_addr_o(6);
    alias slot_A14    is sram_addr_o(7);
    alias slot_A15    is sram_addr_o(14);

    alias slot_D_io       is sram_data_io;

    alias slot_CS1_o      is GPIO(25);
    alias slot_CS2_o      is GPIO(24);
    alias slot_CS12_o     is GPIO(23);
    alias slot_CLOCK_o    is GPIO(10);
    alias slot_M1_o       is GPIO(19);

    alias slot_MREQ_o     is sram_addr_o(16); 
    alias slot_IOREQ_o    is GPIO(18);
    alias slot_RD_o       is GPIO(16);
    alias slot_WR_o       is GPIO(14);
    alias slot_RESET_io   is GPIO(28);

    alias slot_SLOT1_o    is GPIO(21);
    alias slot_SLOT2_o    is GPIO(22);
    alias slot_SLOT3_o    is GPIO(26);
    
    alias slot_BUSDIR_i   is GPIO(31);
    alias slot_RFSH_i     is GPIO(20);
    alias slot_INT_i      is GPIO(27);
    alias slot_WAIT_i     is GPIO(29);

    alias slot_DATA_OE_o  is GPIO(12);
    alias slot_DATA_DIR_o is GPIO(13);

    alias ext_cart_detect is GPIO(5);

    alias esp_tx_i        is GPIO(7);
    alias esp_rx_o        is GPIO(11);

begin

    PumpSignal_1 : PumpSignal port map(clk21m, (not pll_locked) or (not btn_n_i(4)), status(31), pump_s);

    LED         <= (not blink_s);

    sram_oe_n_o <= '1';
    sram_we_n_o <= '1';

    stm_rst_o   <= 'Z';

    joystick_serial1 : joystick_serial 
    port map
    (
        clk_i           => clk21m,
        joy_data_i      => joy_data_i,
        joy_clk_o       => joy_clock_o,
        joy_load_o      => joy_load_o,

        joy1_up_o       => joy1_up_i,
        joy1_down_o     => joy1_down_i,
        joy1_left_o     => joy1_left_i,
        joy1_right_o    => joy1_right_i,
        joy1_fire1_o    => joy1_p6_i,
        joy1_fire2_o    => joy1_p9_i,

        joy2_up_o       => joy2_up_i,
        joy2_down_o     => joy2_down_i,
        joy2_left_o     => joy2_left_i,
        joy2_right_o    => joy2_right_i,
        joy2_fire1_o    => joy2_p6_i,
        joy2_fire2_o    => joy2_p9_i
    );

    reset_s <= btn_n_i(4) and power_on_reset and not status(0);
  
--  U00 : work.pll
--    port map(
--      inclk0   => clock_50_i,              
--      c0       => clk21m,                 -- 21.48MHz internal
--      c1       => memclk,                 -- 85.92MHz = 21.48MHz x 4
--      c2       => pMemClk,                -- 85.92MHz external
--      c3       => clk_hdmi                -- 107.40Mhz = 21.48MHz x 5
--    );

    SDRAM_CLK <= clk_sdram;

    ocm: work.emsx_top
    generic map
    (
        use_wifi_g          => false,
        use_midi_g          => false,
        ZemmixNeo           => '1'
    )
    port map
    (
        -- Clock, Reset ports
        pClk21m         => clock_50_i,
        pExtClk         => '0',
        clk21m_out      => clk21m,
        clk_hdmi        => clk_hdmi,


        pSltRst_n       => reset_s,

        -- SD-RAM ports
        pMemClk         => clk_sdram,      -- SD-RAM Clock
        pMemCke         => SDRAM_CKE,      -- SD-RAM Clock enable
        pMemCs_n        => SDRAM_nCS,      -- SD-RAM Chip select
        pMemRas_n       => SDRAM_nRAS,     -- SD-RAM Row/RAS
        pMemCas_n       => SDRAM_nCAS,     -- SD-RAM /CAS
        pMemWe_n        => SDRAM_nWE,      -- SD-RAM /WE
        pMemUdq         => SDRAM_DQMH,     -- SD-RAM UDQM
        pMemLdq         => SDRAM_DQML,     -- SD-RAM LDQM
        pMemBa1         => SDRAM_BA(1),    -- SD-RAM Bank select address 1
        pMemBa0         => SDRAM_BA(0),    -- SD-RAM Bank select address 0
        pMemAdr         => SDRAM_A,        -- SD-RAM Address
        pMemDat         => SDRAM_DQ,       -- SD-RAM Data

        -- PS/2 keyboard ports
        pPs2Clk         => ps2_clk_io,
        pPs2Dat         => ps2_data_io,

        -- Joystick ports (Port_A, Port_B)
        pJoyA_in(5)     =>  joy1_p9_i,
        pJoyA_in(4)     =>  joy1_p6_i,
        pJoyA_in(3)     =>  joy1_right_i,
        pJoyA_in(2)     =>  joy1_left_i,
        pJoyA_in(1)     =>  joy1_down_i,
        pJoyA_in(0)     =>  joy1_up_i,

        pJoyB_in(5)     =>  joy2_p9_i,
        pJoyB_in(4)     =>  joy2_p6_i,
        pJoyB_in(3)     =>  joy2_right_i,
        pJoyB_in(2)     =>  joy2_left_i,
        pJoyB_in(1)     =>  joy2_down_i,
        pJoyB_in(0)     =>  joy2_up_i,

        pStrA           => open,
        pStrB           => open,

        -- SD/MMC slot ports
        pSd_Ck          => sd_sclk_o,               -- pin 5 Clock
        pSd_Cm          => sd_mosi_o,               -- pin 2 Datain
        pSd_Dt(3)       => sd_cs_n_o,               -- pin 1 CS
        pSd_Dt(2)       => open,
        pSd_Dt(1)       => open,
        pSd_Dt(0)       => sd_miso_i,               -- pin 7 Dataout

        -- DIP switch, Lamp ports
        pDip            => '0' & not status(6) & not status(5 downto 4) & not status(3) & not (status(8)) & status (2) & not status(1),
        pLed(0)         => blink_s,

        -- Video, Audio/CMT ports
        pDac_VR         => vga_r_s,
        pDac_VG         => vga_g_s,
        pDac_VB         => vga_b_s,

        pDac_SL         => SL_s,
        pDac_SR         => SR_s,

        pVideoHS_n      => vga_hsync_n_s,
        pVideoVS_n      => vga_vsync_n_s,

        pcm_o           => PCM_s,
        BLANK_o         => blank_s,

        ear_i           => ear_i,
        mic_o           => mic_o,


          
        -- MSX cartridge slot ports
        pCpuClk         => slot_CLOCK_o, 
          
        pSltAdr         => slot_A_s,
        pSltDat         => slot_D_io,        

        pSltMerq_n      => cpu_mreq_s,
        pSltIorq_n      => cpu_ioreq_s,
        pSltRd_n        => cpu_rd_s,
        pSltWr_n        => slot_WR_o,
          
        pSltRfsh_n      => slot_RFSH_i,
        pSltWait_n      => slot_WAIT_i,
        pSltInt_n       => slot_INT_i,
        pSltM1_n        => slot_M1_o,   
          
        pSltBdir_n      => slot_BUSDIR_i, -- not used
        pSltSltsl_n     => slot_SLOT1_s,
        pSltSlts2_n     => slot_SLOT2_s,
        pSltCs1_n       => slot_CS1_o,
        pSltCs2_n       => slot_CS2_o,
        pSltCs12_n      => slot_CS12_o,
          
        BusDir_o          => BusDir_s,




        --others
        pCpuClk         => open,
        pSltClk         => '0',
        pIopRsv14       => '0',
        pIopRsv15       => '0',
        pIopRsv16       => '0',
        pIopRsv17       => '0',
        pIopRsv18       => '0',
        pIopRsv19       => '0',
        pIopRsv20       => '0',
        pIopRsv21       => '0',

        osd_o           => osd_s,
        pll_locked      => pll_locked
    );

    slot_A0   <= slot_A_s(0);
    slot_A1   <= slot_A_s(1);
    slot_A2   <= slot_A_s(2);
    slot_A3   <= slot_A_s(3);
    slot_A4   <= slot_A_s(4);
    slot_A5   <= slot_A_s(5);
    slot_A6   <= slot_A_s(6);
    slot_A7   <= slot_A_s(7);
    slot_A8   <= slot_A_s(8);
    slot_A9   <= slot_A_s(9);
    slot_A10  <= slot_A_s(10);
    slot_A11  <= slot_A_s(11);
    slot_A12  <= slot_A_s(12);
    slot_A13  <= slot_A_s(13);
    slot_A14  <= slot_A_s(14);
    slot_A15  <= slot_A_s(15);

     
    slot_IOREQ_o <= cpu_ioreq_s; 
    slot_MREQ_o  <= cpu_mreq_s; 
    slot_RD_o    <= cpu_rd_s;
    slot_SLOT1_o <= slot_SLOT1_s;
    slot_SLOT2_o <= slot_SLOT2_s;
    slot_SLOT3_o <= slot_SLOT1_s;
    
    -- RESET to the SLOT pins
    slot_RESET_io <= reset_s;

    -- 74LVC4245
    slot_DATA_OE_o   <= '0' when slot_SLOT1_s = '0' else
                             '0' when slot_SLOT2_s = '0' else
                             '0' when cpu_ioreq_s = '0' and BusDir_s = '0' else 
                             '1';
    slot_DATA_DIR_o <= not cpu_rd_s; -- port A=SLOT, B=FPGA     DIR(1)=A to B   
    
     data_io_inst: data_io
     generic map (STRLEN => CONF_STR'length)
     port map (
       clk_sys  => clk21m,
       SPI_SCK  => SPI_SCK,
       SPI_SS2  => SPI_SS2,
       SPI_DI   => SPI_DI,
       SPI_DO   => SPI_DO,
 
       data_in  => pump_s and osd_s,
       conf_str => to_slv(CONF_STR),
       status   => status
 
     );

     mist_video_inst : mist_video
     generic map (
         OSD_COLOR => "001"
     )
     port map (
         clk_sys     => clk_sdram,
         scanlines   => status(11 downto 10),
         rotate      => "00",
         scandoubler_disable => '1',
         ce_divider  => '0', --1 imagem ok clk21 ou 0 com clksdram para usar blend
         blend       => status(12),
 
         SPI_SCK     => SPI_SCK,
         SPI_SS3     => SPI_SS2,
         SPI_DI      => SPI_DI,
 
         HSync       => vga_hsync_n_s, --esta correto
         VSync       => vga_vsync_n_s, --esta correto
         R           => vga_r_s(5 downto 0),
         G           => vga_g_s(5 downto 0),
         B           => vga_b_s(5 downto 0),
 
         VGA_HS      => VGA_HS,
         VGA_VS      => VGA_VS,
         VGA_R       => VGA_R,
         VGA_G       => VGA_G,
         VGA_B       => VGA_B,
 
         osd_enable  => open
     );

--    osd1 : osd     
--    port map(
--        clk_sys=> clk_sdram,
--        ce=> '0',
--        
--         SPI_SCK     => SPI_SCK,
--         SPI_SS3     => SPI_SS2,
--         SPI_DI      => SPI_DI,
--
--        rotate  => "00", 
--
--        R_in  => vga_r_s,
--        G_in  => vga_g_s,
--        B_in  => vga_b_s,
--        HSync => vga_hsync_n_s,
--        VSync => vga_vsync_n_s,
--
--        R_out(5 downto 1) => VGA_R,
--        G_out(5 downto 1) => VGA_G,
--        B_out(5 downto 1) => VGA_B,
--        osd_enable => open
--    );
--
--    VGA_HS <=vga_hsync_n_s;
--    VGA_VS <= vga_vsync_n_s;

     AUDIO_R <= SR_s(0);
     AUDIO_L <= SL_s(0);
    
        -- power on reset
        process (clk_sdram)
        begin
            if rising_edge(clk_sdram) then
                if power_on_s /= x"00" then
                    power_on_s <= power_on_s - 1;
                    power_on_reset <= '0';
                else
                    power_on_reset <= '1';
                end if;
                
            end if;
        end process;
    
end architecture;
